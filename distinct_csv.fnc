create or replace function distinct_csv(p_csv_string in varchar2, p_delimeter varchar2 default ',') return varchar2 result_cache  as
  
  result_csv varchar2(32767) ;
  

BEGIN
  
  IF p_csv_string IS NULL 
    THEN raise_application_error(-20001,'�������� ������ ������ NULL');
  END IF;
  
  IF p_delimeter IS NULL
    THEN raise_application_error(-20001,'����������� ����� NULL');
  END IF;    

  WITH base AS(
  SELECT (regexp_substr (
           p_csv_string,
           '[^,]+',
           1,
           LEVEL
         )) VALUE
         
  FROM   dual
  CONNECT BY LEVEL <= 
    length ( p_csv_string ) - length ( replace ( p_csv_string, p_delimeter ) ) + 1
  ),
  ranged_base as( 
  SELECT b.*, ROW_NUMBER() OVER( PARTITION BY b.value ORDER BY b.value) rank
  FROM base b)
  
  SELECT listagg(rb.value,p_delimeter) within group (order by rb.rank)
  INTO result_csv
  FROM ranged_base rb
  WHERE rb.rank = 1;
  RETURN(result_csv);
  
  EXCEPTION
    WHEN no_data_found THEN
      raise_application_error(-20001,'��������� - ������ ������');
    WHEN OTHERS THEN
      --here plase to log exception 
      RAISE;
end distinct_csv;
/
