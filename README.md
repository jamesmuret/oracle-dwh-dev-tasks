# Oracle DWH dev tasks

## О чем репозиторий

Примеры моих скриптов для решения высланных задач 


## Скрипт distinct_csv.fnc и distinct_csv_test.tst


- [ ] Скрипт функции, выполняющий разбор строки с разделителем, для удаления дубликатов и пересборки получившейся строки в итоговый результат.

## Скрипт task2

- [ ] Решает задачу по выводу аналитику курсов валют за вчерашний день - столбец yesterday_rate, и построение гистограммы histogramm.
- [ ] Вчерашний курс валюты строится в разрезе окна валюты, и показывает последнее значение за вчерашний день, до 00:00:00 текущего дня. Ранг курса при наличии нескольких курсов одновременно выбирается по минимальному значению курса.
- [ ] Гистограмма курсов строится на шкале 1...70, на который переводится текущее значение курса. Где 1 - минимальный курс валюты за все время, 70 - максимальный.
- [ ] Детали и последовательность стобцов - согласно тайне задания. 

