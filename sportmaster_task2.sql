WITH BASE AS(
SELECT t.dt,
       t.currency,
       ROUND(t.rate,2) rate,             
       ROUND((SELECT MIN(tt.rate)  KEEP (DENSE_RANK LAST ORDER BY tt.dt ASC)
        FROM TMP_MYTABLE tt 
        WHERE tt.dt < trunc(t.dt)
        AND tt.currency = t.currency),2)   yesterday_rate,
        NVL(LPAD('>',
        ROUND((t.rate - MIN(t.rate) OVER (PARTITION BY t.currency))/((MAX(t.rate) 
        OVER (PARTITION BY t.currency) -
        MIN(t.rate) OVER (PARTITION BY t.currency))/70)),'-'),'>')   histogramm
 
FROM TMP_MYTABLE t
)
SELECT b.dt,b.currency,b.rate,b.yesterday_rate,b.histogramm
FROM BASE b
ORDER BY b.currency, b.dt ASC, b.rate DESC

